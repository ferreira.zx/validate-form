import axios from "axios";

// axios.defaults.headers.common["Authorization"] = `Bearer ${localStorage.getItem(
//   "token_auth"
//)}`;

//especifica à api que não precisa de verificação de versão do app...
const versao_app = "web.vue.1.0";
const api = 'https://hom.nolu.io:3000'

export default class Network {
  constructor(store) {
    this.imei = store.state.imei;
    this.store = store;
  }

  obtainStage(success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    console.log(this.store.state, "STATE STORE");
    return axios
      .get(`${this.store.state.base_url_wc}/api/now/stage`)
      .then(function(data) {
        console.log(data, "STAGE");
        success(data.data.result);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  // verificaDebito(token_auth, token, success, error) {
  //   return axios
  //     .post(`${this.store.state.base_url_wc}/`, {
  //       command: "capturarTokenDebito",
  //       versao_app: versao_app,
  //       token_auth: token_auth,
  //       token: token
  //     })
  //     .then(function(data) {
  //       success(data.data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //       error(err);
  //     });
  // }

  // async finalizaDebito(data, success, error) {
  //   return axios
  //     .post(`${this.store.state.base_url_wc}/`, {
  //       ...data,
  //       command: "comprarCertificadoNoDebito",
  //       versao_app: versao_app
  //     })
  //     .then(function(data) {
  //       success(data.data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //       error(err);
  //     });

  // }

  // statusDebito(data, success, error) {
  //   return axios
  //     .post(`${this.store.state.base_url_wc}/`, {
  //       ...data,
  //       command: "atualizarTokenDebito",
  //       versao_app: versao_app
  //     })
  //     .then(function(data) {
  //       success(data.data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //       error(err);
  //     });
  // }

  obtainCards(success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .get(`${this.store.state.base_url_wc}/api/now/certificate`)
      .then(function(data) {
        console.log(data, "CERT");
        success(data.data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  obtainHistory(success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .get(`${this.store.state.base_url_wc}/api/now/lottery`)
      .then(function(data) {
        console.log(data, "history");
        success(data.data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  registerUser(user, success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    // const dt = user.data_nascimento;
    // user.data_nascimento = `${dt[0] + dt[1]}/${dt[2] + dt[3]}/${dt[4] +
    //   dt[5] +
    //   dt[6] +
    //   dt[7]}`;
    axios
      .post(`${this.store.state.base_url_wc}/api/user/complete`, user)
      .then(function(data) {
        success(data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  updateUser(id, data, success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .put(`${this.store.state.base_url_wc}/api/user/complete`, data)
      .then(function(data) {
        console.log(data, "UPDATE USER");
        success(data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  updatePassword(id, data, success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .put(`${this.store.state.base_url_wc}/api/user/password/${id}`, data)
      .then(function(data) {
        console.log(data, "UPDATE USER");
        success(data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  async loginUser(data, success, error) {
    axios
      .post(`${api}/api/login`, data)
      .then(function(data) {
        if (data.data.result.token) {
          localStorage.setItem("token_auth", data.data.result.token);
        }
        success(data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  // recoverPassword(cpf, success, error) {
  //   axios
  //     .post(`${this.store.state.base_url_wc}/`, {
  //       cpf: cpf,
  //       command: "recuperarSenhaPasso1",
  //       versao_app: versao_app
  //     })
  //     .then(function(data) {
  //       success(data.data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //       error(err);
  //     });
  // }

  obtainCreditCards(success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .get(`${this.store.state.base_url_wc}/api/wallet`)
      .then(data => {
        success(data);
      })
      .catch(err => {
        console.log(err);
        error(err);
      });
  }

  // obtainCreditCards(token, success, error) {
  //   axios
  //     .post(`${this.store.state.base_url_wc}/`, {
  //       command: "listarCartoes",
  //       token_auth: token,
  //       versao_app: versao_app
  //     })
  //     .then(data => {
  //       success(data.data);
  //     })
  //     .catch(err => {
  //       console.log(err);
  //       error(err);
  //     });
  // }

  registerCreditCard(data, success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .post(`${this.store.state.base_url_wc}/api/wallet`, data)
      .then(function(data) {
        success(data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  removeCreditCard(data, success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .delete(
        `${this.store.state.base_url_wc}/api/delete_wallet/${data.id}`,
        data.numero_cartao
      )
      .then(function(data) {
        success(data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  // registerCreditCard(data, success, error) {
  //   axios
  //     .post(`${this.store.state.base_url_wc}/`, {
  //       ...data,
  //       command: "adicionarCartao",
  //       versao_app: versao_app
  //     })
  //     .then(function(data) {
  //       success(data.data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //       error(err);
  //     });
  // }

  // confirmCreditCard(data, success, error) {
  //   axios
  //     .post(`${this.store.state.base_url_wc}/api/activecreditcard`, data)
  //     .then(function(data) {
  //       success(data.data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //       error(err);
  //     });
  // }

  registerPurchase(data, success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .post(`${this.store.state.base_url_wc}/api/cielo/transaction`, data)
      .then(function(data) {
        success(data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  obtainPurchases(success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      // .get(`${this.store.state.base_url_wc}/api/compras?token_auth=${token}`)
      .get(`${this.store.state.base_url_wc}/api/now/certificate/user`)
      .then(function(data) {
        console.log(data, "PURCHASES");
        success(data.data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  requestResetPassword(cpf, success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .post(`${this.store.state.base_url_wc}/api/forgot`, cpf)
      .then(function(data) {
        console.log(data);
        success(data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  confirmResetPassword(data, success, error) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      this.store.state.token_auth
    }`;
    axios
      .post(`${this.store.state.base_url_wc}/api/auth/password`, data)
      .then(function(data) {
        success(data);
      })
      .catch(function(err) {
        console.log(err);
        error(err);
      });
  }

  // checkCPF(cpf, success, error) {
  //   axios
  //     .post(`${this.store.state.base_url_wc}/`, {
  //       command: "buscaCpf",
  //       versao_app: versao_app,
  //       cpf: cpf
  //     })
  //     .then(function(data) {
  //       success(data.data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //       error(err);
  //     });
  // }

  // loginFacebook(data, success, error) {
  //   axios
  //     .post(`${this.store.state.base_url_wc}/`, {
  //       command: "loginWithFacebook",
  //       versao_app: versao_app,
  //       ...data
  //     })
  //     .then(function(data) {
  //       success(data.data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //       error(err.response);
  //     });
  // }

  // async lotteryActive(token) {

  //   let res = await axios.post(`${this.store.state.base_url_wc}/`,
  //     {
  //       "command": "certificadosVigentes",
  //       "token_auth": token,
  //       "versao_app": versao_app
  //     }
  //   )
  //   return res;
  // }
}
