import  jwt from '~/plugins/jsonwebtoken';

export default function({ store, redirect }) {
    /**
     * vamos usar o `parsedUrl` para acessar a url e atribuir uid e token
     * à uma respectiva variavel que será tratada aqui 
     * se válida o user é redirecionado normalmente para && o store recebe o 
     * status appConfirm = true // uid // token
     * confirmar dados vindo do app 
     * se não o usuário nem verá essa tela e será redirecionado para a rota
     */
    var parsedUrl = new URL(window.location.href) // acess the url and insert into var
    var hash = parsedUrl.searchParams.get('token')

    function decoded() {
        var decoded = jwt.verify(hash, 'L71TqC6p59N2Ym7hNhXw7wdtG2011ij5');
        store.dispatch('getUser', decoded)
        console.log(decoded)
    }

    !hash ? redirect ('/') : decoded()
}