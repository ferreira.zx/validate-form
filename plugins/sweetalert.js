import swal from "sweetalert2";

export default (context, inject) => {
  inject("swal", swal);
};
