export const state = () => ({
    user: {},
    appConfirm: false,
    uid: null,
    api: null,
    token: null,
    feedback: '',
})

export const mutations = {
    setUser (state, value) {
        console.log(value)
        state.user = value
    },
    setConfig (state, value) {
        state.uid = value.user
        state.api = value.apiUrl,
        state.token = value.token
    },
    setFeedback (state, value) {
        state.feedback = value
    },
    eraseFeedback (state, value) {
        state.feedback = value
    }
}

export const actions = {
    getUser({ commit }, payload) {
        commit('setConfig', payload)
        const AuthStr = 'Bearer '.concat(payload.token);
        this.$axios.get(`${payload.apiUrl}user/${payload.user}`, { headers: { Authorization: AuthStr }})
            .then((res) => {         
            commit('setUser', res.data[0])
        })
    },
    updateUser ({ commit }, payload) {
        const AuthStr = 'Bearer '.concat(this.state.token);
        this.$axios.put(`${this.state.api}form/update/${this.state.uid}`, payload)
          .then((res) => {
            if (res.status == 200) {
                commit('setFeedback', true)
            } else {
                commit('setFeedback', 401)
            }
          }).catch((error) =>{
            commit('setFeedback', false)
              console.log('error: ', error.status)
          })
    },
    sendSMS ({ commit }, payload) {
        this.$axios.$post(`${this.state.api}twilio`, {
            telefone: payload.telefone,
            email: payload.email,
            cpf: payload.cpf
        }).then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
    }
}